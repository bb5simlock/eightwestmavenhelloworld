package ie.eightwest;



import ie.eightwest.userdao.*;

import java.util.Collection;

/**
 * Hello world!
 *
 */
public class App {

    public static void main( String[] args ) {

        UserDao udb = new UserDaoSQLite();
        Collection<User> users = udb.getUsers();

        for(User user: users){
            System.out.println(user);
        }

        try {
            udb.addUser(new User(77,"new user", "email.new user", true));
//            udb.addUser(new User(99,"new user", "email.new user", true));
        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        users = udb.getUsers();
        for(User user: users){
            System.out.println(user);
        }
        System.out.println("-------------------------------------");
        System.out.println("\n---------UPDATING-------------------");
        try {
            User user = udb.getUser(3l);
            System.out.println(user);
            user.setName("changed now");
            udb.updateUser(user);
            System.out.println(udb.getUser(3l));

        } catch (UserDaoException e) {
            e.printStackTrace();
        }

        System.out.println("-------------------------------------");

        try {
            System.out.println(udb.getUser(4));
            System.out.println(udb.getUser(400));
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
    }
}
