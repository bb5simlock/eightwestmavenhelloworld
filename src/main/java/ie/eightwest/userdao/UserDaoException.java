package ie.eightwest.userdao;

public class UserDaoException extends Exception {

    public UserDaoException(String message) {
        super(message);
    }
}
