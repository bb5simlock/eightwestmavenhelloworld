package ie.eightwest;

import ie.eightwest.userdao.User;
import ie.eightwest.userdao.UserDao;
import ie.eightwest.userdao.UserDaoException;
import ie.eightwest.userdao.UserDaoInMemory;
import org.junit.Assert;
import org.junit.Test;

import javax.jws.soap.SOAPBinding;

import static org.junit.Assert.*;

/**
 * Unit test for simple App.
 */
public class AppTest 
{



    @Test(expected = UserDaoException.class)
    public void getNonExistentUser() throws UserDaoException {
        UserDao udb = new UserDaoInMemory();
        User u = udb.getUser(999);
    }

    @Test(expected = UserDaoException.class)
    public void deleteNonExistingUser() throws UserDaoException {
        UserDao udb = new UserDaoInMemory();
        udb.deleteUser(999);
    }

}
