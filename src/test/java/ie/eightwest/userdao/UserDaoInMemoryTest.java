package ie.eightwest.userdao;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserDaoInMemoryTest {

    @Test
    public void createDao() {
        UserDaoInMemory udb = new UserDaoInMemory();
        Assert.assertNotNull(udb);
    }

    @Test(expected = UserDaoException.class)
    public void deleteNonExistentUser() throws UserDaoException {
        UserDaoInMemory udb = new UserDaoInMemory();
        udb.deleteUser(9999);
    }

}