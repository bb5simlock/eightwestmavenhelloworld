package ie.eightwest.userdao;

import org.junit.*;

import static org.junit.Assert.*;

public class UserDaoSQLiteTest {

    UserDaoSQLite udb;

    public UserDaoSQLiteTest() {
        udb = new UserDaoSQLite("c:/Users/HP201802/Desktop/SQLiteDB/testusers.db");
        //"c:/Users/HP201802/Desktop/SQLiteDB/testusers.db"
        try {
            udb.deleteUser(1);
            udb.deleteUser(2);
            udb.deleteUser(3);
            udb.addUser(new User(1,"test1", "email1", true));
            udb.addUser(new User(2,"test2", "email2", true));
            udb.addUser(new User(3,"test3", "email3", true));
        } catch (UserDaoException e) {
            e.printStackTrace();
        }
    }

    @Before
    public void before(){
        System.out.println("before() called");
    }

    @After
    public void after(){
        System.out.println("after() called");
    }

    @BeforeClass
    public static void beforeClass(){
        System.out.println("before class called");
    }

    @AfterClass
    public static void afterClass(){
        System.out.println("after class called");
    }
//----------------------------------------------------------------------------------------------------------------------
    @Test
    public void createDao() {

        Assert.assertNotNull(udb);
    }

    @Test(expected = UserDaoException.class)
    public void deleteNonExistentUser() throws UserDaoException {
        udb.deleteUser(9999);
    }

    @Test(expected = UserDaoException.class)
    public void createUserWithIdThatAlreadyExists() throws UserDaoException {
        udb.addUser(new User(1,"test1", "email1", true));
    }
}