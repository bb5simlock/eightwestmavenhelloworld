package ie.eightwest.userdao;

import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {
    @Test
    public void checkIdMinusOne(){
        User u = new User("marcin", "email", true);
        assertTrue(u.getId()==-1);
    }

}